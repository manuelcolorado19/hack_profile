Rails.application.routes.draw do

  get 'friends/index'
  get 'friends/show'
  get 'friends/new'
  get 'friends/create'
  get 'friends/update'
  get 'friends/destroy'
  resources :profiles do
    resources :certificates
    resources :hobbies
    resources :friends
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
