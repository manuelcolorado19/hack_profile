class CreateProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :profiles do |t|
      t.string :name
      t.string :last_name
      t.string :subtititle
      t.string :email
      t.string :user_name
      t.string :bio
      t.date :birthday

      t.timestamps
    end
  end
end
