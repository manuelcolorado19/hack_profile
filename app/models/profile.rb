class Profile < ApplicationRecord
    has_many :hobbies
    has_many :certificates
    has_many :friends
    accepts_nested_attributes_for :hobbies, reject_if: :all_blank
    accepts_nested_attributes_for :certificates, reject_if: :all_blank
    has_one_attached :image



   
end
