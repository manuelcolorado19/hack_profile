class ProfilesController < ApplicationController
  before_action :set_profile, except: [:index, :new, :create]

  def index 
    if @profile_db = Profile.first
       @endpoint = params[:url] 
      if params[:url]
       parser 
      else
        hash_builder
        respond_to do |format|
          format.json { render json: @profile_db }
          format.html 
        end  
      end
    else 
      redirect_to new_profile_path
    end
  end 
  

  def new
    @profile = Profile.new
    @profile.hobbies.new
    @profile.certificates.new
  end 

  def create
    @profile = Profile.new(profile_params)
    @profile.save
    redirect_to @profile, notice: "Customer successfully created"
  end

  def edit 
  end

  def update
    if @profile.update profile_params 
      redirect_to profiles_path
    else 
      render :edit
    end
  end

  def show 
    if @profile
      respond_to do |format|
        format.json { render json: @profile }
        format.html 
      end       
    else
      redirect_to new_profile_path
    end
  end

  def destroy
    @profile.destroy
    redirect_to profiles_path
  end


  private 

  def set_profile
    @profile = Profile.find params[:id]
  end

  def hash_builder 
    @amigos = []
    suma = 0 
    @profile_db.friends.each do |amigo|
      @endpoint = amigo.url
      @amigos[suma] = parser, amigo.url
      suma+=1
    end
    # @certs = []
    # cont = 0 
    # @profile_db.certificates.each do |d|
    #   @certs[cont] = d.name, url_for(d.pdf)
    #   cont+=1
    # end

    @profile = { "profile" => {
      "firstname" => @profile_db.name,
      "lastname" => @profile_db.last_name,
      "subtitle" => @profile_db.subtititle,
      "birthday" => @profile_db.birthday,
      "email" => @profile_db.email,
      "friends" => @amigos,
      "bio" => @profile_db.bio,
      "hobbies" => @profile_db.hobbies,
      "certificates" => @profile_db.certificates,
      "image" => @profile_db.image
    }}
  end

  def parser
    @hash = RestClient.get @endpoint
    @profile = JSON.parse @hash
  end 

  def set_profile 
    @profile = Profile.find params[:id]
  end

  def profile_params
    params.require(:profile).permit(:name, :last_name, :bio, :birthday, :email, :image, :subtititle, :user_name, hobbies_attributes: [:name, :descripticon, :preference, :category], certificates_attributes: [:name,:pdf])
  end 

  
end
