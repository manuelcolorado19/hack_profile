class CertificatesController < ApplicationController
  before_action :set_profile, execept: [:index,:show]

  def index
  end

  def show
  end

  def new
    @cert = @profile.certificates.build
  end

  def create
    @cert = @profile.certificates.build cert_params
    @cert.save
    redirect_to profiles_path
  end
  
  def edit
    @cert = @profile.certificates.find params[:id]
  end

  def update
    @cert = @profile.certificates.find params[:id]
    if @cert.update cert_params 
      redirect_to profiles_path, notice: "cert succesfully updated"
    else
      @cert.update cert_params
      render :edit
    end
  end

  def destroy
    @certificate = @profile.certificates.find params[:id]
    @certificate.destroy
    redirect_to profiles_path
  end

  private
  def set_profile
    @profile = Profile.find params[:profile_id]
  end

  def cert_params 
    params.require(:certificate).permit(:name,:pdf)
  end
end
