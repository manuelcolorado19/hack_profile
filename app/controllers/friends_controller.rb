class FriendsController < ApplicationController
  before_action :set_profile, execept: :index

  def index
  end

  def show
  end

  def new
    @profile = Profile.find params[:profile_id]
    @friend = @profile.friends.build
  end

  def create
    @profile.friends.build friend_params
    if @profile.save
      redirect_to profiles_path
    end
    
  end

  def edit
    @profile = Profile.find params[:profile_id]
    @friends = @profile.friends.find params[:id]
  end

  def update
    @profile = Profile.find params[:profile_id]
    @friends = @profile.friends.find params[:id]
    if @friends.update friend_params
      redirect_to profiles_path
    end
  end


  def destroy
    @profile = Profile.find params[:profile_id]
    @friend = @profile.friends.find params[:id]
    @friend.destroy
    redirect_to profiles_path
  end

  private 
  def set_profile
    @profile = Profile.find params[:profile_id]
  end

  def friend_params
    params.require(:friend).permit(:aka, :url) 
  end 

end
