class HobbiesController < ApplicationController
  before_action :set_profile

  def index
  end

  def show
  end

  def new
    @hobbies = @profile.hobbies.build
  end

  def create
    @hobby = @profile.hobbies.build hobby_params
    @hobby.save
    redirect_to profiles_path
  end 

  def edit
    @hobby = @profile.hobbies.find params[:id]
  end

  def update
    @hobbies = @profile.hobbies.find params[:id]
    if @hobbies.update hobby_params
      redirect_to profiles_path
    end
  end

  def destroy
    @hobby = @profile.hobbies.find params[:id]
    @hobby.destroy
    redirect_to profiles_path
  end

  private

  def hobby_params
    params.require(:hobby).permit(:name,:descripticon,:preference,:category) 
  end 

  def set_profile
    @profile = Profile.find params[:profile_id]
  end
  
end
