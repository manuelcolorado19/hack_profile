class ProfileSerializer < ActiveModel::Serializer
  attributes :firstname , :lastname, :image, :bio, :hobbies, :certificates
  has_many :hobbies
  has_many :certificates

  def image
    url_for(object.image) if object.image.attachment
  end

  def firstname 
    object.name
  end

  def lastname 
    object.last_name
  end

  def subtitle
    object.subtititle
  end
end
